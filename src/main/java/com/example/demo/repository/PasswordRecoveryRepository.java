package com.example.demo.repository;


import com.example.demo.datasource.PasswordRecovery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface PasswordRecoveryRepository extends JpaRepository<PasswordRecovery,Integer> {

    PasswordRecovery findByUid(String uid);
}
