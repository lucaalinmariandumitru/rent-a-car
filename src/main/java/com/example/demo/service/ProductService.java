//package com.example.demo.service;
//
//import com.example.demo.datasource.Product;
//import com.example.demo.exception.FiledIsMandatoryException;
//import com.example.demo.repository.ProductRepository;
//import org.apache.logging.log4j.util.Strings;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.util.List;
//
//@Service
//public class ProductService {
//
//    @Autowired
//    private ProductRepository productRepository;
//
//    public List<Product> findAll(String name) {
//        if (Strings.isEmpty(name)) {
//            return productRepository.findAll();
//        }
//        return productRepository.findByName(name);
//    }
//
//    public Product create(Product product) {
//        validate(product);
//        return productRepository.save(product);
//    }
//
//    private void validate(Product product) {
//        if (Strings.isEmpty(product.getName())) {
//            throw new FiledIsMandatoryException(" Name is mandatory");
//        }
//
//        if (product.getQuantity() == null) {
//            throw new FiledIsMandatoryException("Quantity is mandatory");
//        }
//
//        if (product.getPrice() == null) {
//            throw new FiledIsMandatoryException("Price is mandatory");
//        }
//    }
//
//}
