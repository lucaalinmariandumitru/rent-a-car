package com.example.demo.service;

import com.example.demo.datasource.PasswordRecovery;
import com.example.demo.datasource.Role;
import com.example.demo.datasource.User;
import com.example.demo.dto.ResetPassword;
import com.example.demo.exception.FiledIsMandatoryException;
import com.example.demo.exception.UserNotFoundException;
import com.example.demo.repository.PasswordRecoveryRepository;
import com.example.demo.repository.RoleRepository;
import com.example.demo.repository.UserRepository;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

@Service

public class UserService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordRecoveryRepository passwordRecoveryRepository;


    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public User create(User user) {

        validate(user);

        long count = userRepository.countByEmail(user.getEmail());
        if (count > 0) {
            throw new RuntimeException("Email already in used");
        }

        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));

        return userRepository.save(user);
    }


    public String forgetPassword(String email) {

        User user = userRepository.findByEmail(email);
        if (user == null) {
            throw new RuntimeException("User not found");
        }

        String uuid = UUID.randomUUID().toString();
        PasswordRecovery passwordRecovery = new PasswordRecovery();
        passwordRecovery.setDate(new Date());
        passwordRecovery.setUserId(user.getId());
        passwordRecovery.setUid(uuid);

        passwordRecoveryRepository.save(passwordRecovery);

        return uuid;
    }

    public void resetPassword(ResetPassword resetPassword) {

        PasswordRecovery passwordRecovery =
                passwordRecoveryRepository.findByUid(resetPassword.getUid());

        if (passwordRecovery == null) {
            throw new RuntimeException("Bad request");
        }

        User user = userRepository.findById(passwordRecovery.getUserId()).get();
        if (user == null) {
            throw new RuntimeException("Bad request");
        }

        user.setPassword(bCryptPasswordEncoder.encode(resetPassword.getNewPassword()));
        userRepository.save(user);
        passwordRecoveryRepository.delete(passwordRecovery);

    }


    private void validate(User user) {
        if (Strings.isEmpty(user.getFirstName())) {
            throw new FiledIsMandatoryException("First Name is mandatory");
        }

        if (Strings.isEmpty(user.getLastName())) {
            throw new FiledIsMandatoryException("Last Name is mandatory");
        }


        if (Strings.isEmpty(user.getEmail())) {
            throw new FiledIsMandatoryException("Email is mandatory");
        }
    }




    public User update(Integer id, User user) {
        User dbUser = userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException());

        dbUser.setFirstName(user.getFirstName());
        dbUser.setLastName(user.getLastName());
        return userRepository.save(dbUser);
    }

    public void delete(Integer id) {
        userRepository.deleteById(id);
    }

    public void promote(Integer id) {
        User dbUser = userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException());

        Role role = new Role();
        role.setUserId(dbUser.getId());
        role.setRole("EMPLOYEE");

        roleRepository.save(role);
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return null;
    }
}
