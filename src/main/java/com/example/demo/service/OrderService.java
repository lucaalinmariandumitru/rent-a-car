//package com.example.demo.service;
//
//import com.example.demo.datasource.Order;
//import com.example.demo.datasource.Product;
//import com.example.demo.datasource.User;
//import com.example.demo.exception.OutOfStockException;
//import com.example.demo.exception.ProductNotFoundException;
//import com.example.demo.repository.OrderRepository;
//import com.example.demo.repository.ProductRepository;
//import com.example.demo.repository.UserRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.stereotype.Service;
//
//import java.util.Date;
//import java.util.List;
//import java.util.Optional;
//
//@Service
//public class OrderService {
//
//    @Autowired
//    private ProductRepository productRepository;
//    @Autowired
//    private UserRepository userRepository;
//    @Autowired
//    private OrderRepository orderRepository;
//
//
//    public Order buy(com.example.demo.dto.Order order) {
//
//        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext()
//                .getAuthentication().getPrincipal();
//        User currentUser = userRepository.findByEmail(userDetails.getUsername());
//
//        Optional<Product> optionalProduct = productRepository.findById(order.getProductId());
//        if (!optionalProduct.isPresent()) {
//            throw new ProductNotFoundException();
//        }
//
//        Product product = optionalProduct.get();
//
//        if (product.getQuantity() < order.getQuantity()) {
//            throw new OutOfStockException();
//        }
//
//
//        product.setQuantity(product.getQuantity() - order.getQuantity());
//        productRepository.save(product);
//
//        Order entity = new Order();
//        entity.setProductId(product.getId());
//        entity.setUserId(currentUser.getId());
//        entity.setDate(new Date());
//
//        return orderRepository.save(entity);
//    }
//
//    public List<Order> findCurrentOrders() {
//        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext()
//                .getAuthentication().getPrincipal();
//        User currentUser = userRepository.findByEmail(userDetails.getUsername());
//
//        return orderRepository.findByUserId(currentUser.getId());
//    }
//
//    public List<Order> findAll() {
//        return orderRepository.findAll();
//    }
//}
