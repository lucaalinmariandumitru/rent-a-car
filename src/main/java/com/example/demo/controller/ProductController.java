//package com.example.demo.controller;
//
//import com.example.demo.datasource.Product;
//import com.example.demo.service.ProductService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.access.annotation.Secured;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.List;
//
//@Controller
//@RequestMapping("/api/products")
//public class ProductController {
//
//    @Autowired
//    private ProductService productService;
//
//    @GetMapping("/findAll")
//    public List<Product> findAll(@RequestParam(name = "name", required = false)
//                                             String name) {
//        return productService.findAll(name);
//    }
//
//    @Secured("ROLE_EMPLOYEE")
//    @PostMapping
//    public Product create(@RequestBody Product product) {
//        return productService.create(product);
//    }
//
//}
