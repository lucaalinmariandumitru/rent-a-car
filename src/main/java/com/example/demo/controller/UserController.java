package com.example.demo.controller;

import com.example.demo.datasource.User;
import com.example.demo.dto.ResetPassword;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping
    public User create(@RequestBody User user) {
        return userService.create(user);
    }

    @PostMapping("/forgetPassword/{email}")
    public String forgetPassword(@PathVariable(name = "email") String email) {
        return userService.forgetPassword(email);
    }

    @PostMapping("/resetPassword")
    private void resetPassword(@RequestBody ResetPassword resetPassword) {
        userService.resetPassword(resetPassword);
    }



    @Secured("ROLE_EMPLOYEE")
    @DeleteMapping("/{id}")
    public void delete(@PathVariable(name = "id") Integer id) {
        userService.delete(id);
    }

    @Secured("ROLE_EMPLOYEE")
    @PutMapping("/{id}")
    public User update(
            @PathVariable(name = "id") Integer id,
            @RequestBody User user) {
        return userService.update(id, user);
    }

    @Secured("ROLE_EMPLOYEE")
    @PutMapping("/promote/{id}")
    public void promote(
            @PathVariable(name = "id") Integer id) {
        userService.promote(id);
    }
}
