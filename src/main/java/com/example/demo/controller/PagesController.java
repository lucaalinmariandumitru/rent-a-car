package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PagesController {
    @GetMapping("/home")
    public String home(Model model) {
        return "homepage";
    }

    @GetMapping("/login")
    public String login(Model model) {
        return "login";
    }

    @GetMapping("/aboutUs")
    public String about(Model model) {
        return "aboutUs";
    }

    @GetMapping("/categories")
    public String categories(Model model) {
        return "categories";
    }

    @GetMapping("/userRegistration")
    public String userR(Model model) {
        return "userRegistration";
    }

    @GetMapping("/products")
    public String product(Model model) {
        return "products";
    }

    @GetMapping("/contact")
    public String contact(Model model) {
        return "contact";
    }

    @GetMapping("/search")
    public String search(Model model) {
        return "search";
    }
    @GetMapping("/upload")
    public String upload(Model model) {
        return "uploadForm";
    }
}
